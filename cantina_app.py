import tkinter as tk
from tkinter import simpledialog, messagebox
import json
from datetime import datetime

class Produto:
    def __init__(self, nome, preco):
        self.nome = nome
        self.preco = preco

class Aluno:
    def __init__(self, nome):
        self.nome = nome
        self.divida = 0
        self.registros = []  # Lista de registros de dívida (data, hora, valor, produto)
        self.produtos = []  # Lista de produtos associados ao aluno

class CantinaApp:
    def __init__(self, root):
        self.root = root
        self.root.title("Cantina do Baiano")

        self.alunos = []
        self.label = tk.Label(root, text="Bem-vindo à Cantina!")
        self.label.pack()

        self.create_student_button = tk.Button(root, text="Criar Aluno", command=self.create_student)
        self.create_student_button.pack()

        self.register_debt_button = tk.Button(root, text="Registrar Dívida", command=self.register_debt)
        self.register_debt_button.pack()

        self.list_students_button = tk.Button(root, text="Listar Alunos", command=self.list_students)
        self.list_students_button.pack()

        self.total_debt_button = tk.Button(root, text="Ver Total da Dívida", command=self.total_debt)
        self.total_debt_button.pack()

        self.zero_debt_button = tk.Button(root, text="Zerar Dívida", command=self.zero_debt)
        self.zero_debt_button.pack()

        self.view_records_button = tk.Button(root, text="Ver Registros", command=self.view_records)
        self.view_records_button.pack()

        self.save_button = tk.Button(root, text="Salvar Alunos", command=self.save_students)
        self.save_button.pack()

        self.load_students()  # Carrega alunos salvos ao iniciar

    def create_student(self):
        nome = simpledialog.askstring("Criar Aluno", "Digite o nome do aluno:")
        if nome:
            aluno = Aluno(nome)
            self.alunos.append(aluno)
            messagebox.showinfo("Aluno Criado", f"Aluno {nome} criado com sucesso!")

    def register_debt(self):
        aluno_nome = simpledialog.askstring("Registrar Dívida", "Digite o nome do aluno:")
        aluno = self.find_aluno_by_nome(aluno_nome)
        if aluno:
            divida = float(simpledialog.askstring("Registrar Dívida", "Digite o valor da dívida:"))
            agora = datetime.now()  # Obter a data e hora atual
            aluno.registros.append((agora.date(), agora.time(), divida))  # Adicionar o registro à lista
            aluno.divida += divida
            messagebox.showinfo("Dívida Registrada", f"Dívida de R$ {divida:.2f} registrada para {aluno.nome}!")

    def view_records(self):
        aluno_nome = simpledialog.askstring("Ver Registros", "Digite o nome do aluno:")
        aluno = self.find_aluno_by_nome(aluno_nome)
        if aluno:
            registros_str = ""
            for data, hora, valor in aluno.registros:
                registros_str += f"\nData: {data}, Hora: {hora}, Valor: R$ {valor:.2f}"
            if registros_str:
                messagebox.showinfo("Registros", f"Registros de dívida para {aluno.nome}:\n{registros_str}")
            else:
                messagebox.showinfo("Registros", f"Sem registros de dívida para {aluno.nome}")

    def zero_debt(self):
        aluno_nome = simpledialog.askstring("Zerar Dívida", "Digite o nome do aluno:")
        aluno = self.find_aluno_by_nome(aluno_nome)
        if aluno:
            aluno.divida = 0
            messagebox.showinfo("Dívida Zerada", f"Dívida de {aluno.nome} foi zerada!")

    def save_students(self):
        file_path = "c:/Users/ander/OneDrive/�rea de Trabalho/git/cantina/alunos_data.json"  # Substitua pelo caminho absoluto correto
        with open(file_path, "w") as file:
            data_to_save = [{"nome": aluno.nome, "divida": aluno.divida, "registros": aluno.registros} for aluno in self.alunos]
            json.dump(data_to_save, file, default=self.convert_to_dict)
        messagebox.showinfo("Alunos Salvos", "Informações dos alunos foram salvas com sucesso!")

    def load_students(self):
        file_path = "c:/Users/ander/OneDrive/�rea de Trabalho/git/cantina/alunos_data.json"  # Substitua pelo caminho absoluto correto
        try:
            with open(file_path, "r") as file:
                data = json.load(file)
                for aluno_data in data:
                    aluno = Aluno(aluno_data["nome"])
                    aluno.divida = aluno_data["divida"]
                    aluno.registros = aluno_data["registros"]
                    self.alunos.append(aluno)
        except FileNotFoundError:
            messagebox.showerror("Erro", "Arquivo não encontrado!")

if __name__ == "__main__":
    root = tk.Tk()
    app = CantinaApp(root)
    root.mainloop()
